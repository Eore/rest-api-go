package main

import (
	"log"
	"net/http"

	"./api"
)

func server() {
	http.HandleFunc("/api/user", api.UserAPI)
	log.Fatal(http.ListenAndServe(":8000", nil))
}

func main() {
	// config := libs.LoadConfig("./configs/database.json")
	// fmt.Printf("%d\n", config.Port)
	server()
}
